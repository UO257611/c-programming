
#include <stdio.h>
#include "game_functions.h"
#include <stdbool.h>

void display(int known_location_info[][DIM], int size){
	
	char letter[4] = {'A','B','C','D'};
	int i;
	int j;
	printf("   A B C D\n");
	for( i = 0; i< size; i++)
	{
		printf("%c: ", letter[i]);
		for ( j= 0; j< size; j++)
		{

			if( known_location_info[i][j] == UNKNOWN)
			{
				printf("* ");
			}
			else if ( known_location_info[i][j] == BOMB)
			{
				printf("B ");
		
			}
			else
			{
				printf("%d ", known_location_info[i][j]);
			}
		}	
		
		printf("\n");
	}
	

}


void update_known_info(int row, int col, int bomb_info[][DIM], int known[][DIM]){

	known[row][col] = bomb_info[row][col];
	

}


void check_found(int row, int col, struct locations bombs[], int size, bool* found){
	
	int i;
	int counter = 0;
	for(i=0; i< size; i++)
	{
		if( bombs[i].y == row && bombs[i].x == col && !bombs[i].found)
		{
			bombs[i].found = true;
			*found =true;
		}

	}

}

char get_user_char() 
{
    char res = getchar();
    bool finish = false;
    char dummy_char = ' ';

    while (finish == false) {
        dummy_char = getchar();
        if (dummy_char == '\n')
            finish = true;
    }
    return res;

}
void start_game(struct locations *   bombs, int bomb_location_info[][DIM], int size_of_grid, int players_info[][DIM], int no_of_bombs, int MAX_TRIES)
{
        enum game_status { STILL_ALIVE, GAME_OVER} game_status;
	game_status = STILL_ALIVE;
	int tries =0;
	int row = -1;
	int col = -1;
	bool found = false;
	while(game_status == STILL_ALIVE && tries < MAX_TRIES )
	{
		display( players_info, size_of_grid);

		printf("The number of bombs planted: %d \n", no_of_bombs);
		printf("Number of locations tried: %d \n", tries);
	
		do{	
			printf("Write the row letter:\n");
			char rowLetter = get_user_char();
			row = transformLetter(rowLetter, size_of_grid);

			printf("Write the column letter:\n");
			char colLetter = get_user_char();
			col = transformLetter(colLetter, size_of_grid);
			if(row < 0&& col < 0)
			{
				printf("You tried an invalid row or column \n");
			}
		
		} while( row == -1 || col == -1);

		
	
		check_found(row, col, bombs, no_of_bombs, &found);
		if( found )
		{
			printf("--------------------\n");
			printf("|*|* BOOOOOOOOM *|*|\n");
			printf("--------------------\n");
			game_status= GAME_OVER;
		}
		else
		{
			update_known_info(row,col, bomb_location_info, players_info);
			tries++;
		}	


	}
	
	int i;
	int nBombsFound = 0;
	for(i= 0; i<no_of_bombs && game_status== STILL_ALIVE ; i++)
	{
		display( players_info, size_of_grid);
                printf("The number of bombs planted: %d \n", no_of_bombs);
                printf("Number of locations tried: %d \n", tries);



		printf("Write the row letter of the bomb location:\n");
                char rowLetter = get_user_char();
                int row = transformLetter(rowLetter, size_of_grid);

                printf("Write the column letter of the bomb location:\n");
                char colLetter = get_user_char();
                int col = transformLetter(colLetter, size_of_grid);
                if(row < 0&& col < 0)
                {
                        printf("You tried an invalid row or column \n");
                }
		else
		{
			  check_found(row, col, bombs, no_of_bombs, &found);

			  if (!found)
			  {
				  printf("That was a wrong location \n");
				  game_status = GAME_OVER;
			  }
			 else
			 {
				 printf("BOMB FOUND AND DEFUSED !!! \n");
				 nBombsFound++;
				 players_info[row][col] = BOMB;
				 display( players_info, size_of_grid);
			 } 
                     

		}

		
	}

	if(game_status == STILL_ALIVE)
		printf("CONGRATULATIONS, YOU WON !!!! \n");
	else
		printf("GAME OVER, YOU LOST !!!! \n");
		
}


bool checkNotBoom(int tempx, int tempy , struct locations bombs[], int nBombs)
{
	
	int i;
	for(i=0; i< nBombs; i++)
	{
		if(bombs[i].x == tempx && bombs[i].y== tempy)
			return true;
	}
	return false;
}


int transformLetter(char letter, int size_of_matrix)
{
	int id = letter - 'A';
	if (id>= size_of_matrix)
	{
		return -1;
	}else
	{
		return id;
	}

}
bool checkIfBombsWhereRepeated(int x , int y, struct locations *   bombs, int positionInTheArray)
{
	for(int i=0; i< positionInTheArray; i++)
	{
		if(bombs[i].x == x && bombs[i].y == y)
		{
			return true;
		}
	}
	return false;
}


void generateGrid( struct locations * bombs, int bomb_location_info[][DIM], int n_of_bombs)
{

	int bomb_location_big_copy[DIM+2][DIM+2];
	
	for(int i= 0; i< DIM+2; i++)
	{
		for( int j=0; j<DIM+2; j++)
		{
			bomb_location_big_copy[i][j] = 0;
		}
	}

	for (int i = 0; i< n_of_bombs; i++)
        {
                bomb_location_big_copy[bombs[i].y+1][bombs[i].x+1] = BOMB;
        	
		for(int surround_col = bombs[i].x; surround_col<= (bombs[i].x+2); surround_col++)
		{
			for(int surround_row = bombs[i].y; surround_row<= (bombs[i].y+2); surround_row++)
			{
				if( bomb_location_big_copy[surround_row][surround_col] != BOMB)
					bomb_location_big_copy[surround_row][surround_col]++;
			}
		}
	}

	for(int i= 1; i<=DIM+1; i++)
	{
		for( int j = 1; j<= DIM+1; j++)
		{
			bomb_location_info[i-1][j-1] = bomb_location_big_copy[i][j];
		}
	}


}


