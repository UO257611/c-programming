




#define DIM 4
#define UNKNOWN -1
#define BOMB -2
#include <stdbool.h>
struct locations{
	int x;
	int y;
	bool found;
};

bool checkNotBoom(int tempx, int tempy , struct locations bombs[], int nBombs);
void display(int known_location_info[][DIM], int size);
void update_known_info(int row, int col, int bomb_info[][DIM], int known[][DIM]);
void check_found(int row, int col, struct locations bombs[], int size, bool* found);
void start_game(struct locations *   bombs, int bomb_location_info[][DIM], int size_of_grid, int players_info[][DIM], int no_of_bombs, int MAX_TRIES);
char get_user_char();
int transformLetter(char letter, int size_of_matrix);
bool checkIfBombsWhereRepeated(int x , int y, struct locations *   bombs, int positionInTheArray);
void generateGrid( struct locations * bombs , int bomb_location_info[][DIM], int no_of_bombs);
