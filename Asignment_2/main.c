
#include <stdlib.h>
#include "game_functions.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <time.h> 

void main(int argc, char *argv[]){
        int i;
        int j;	
	srand ( time(NULL) );	
	int n_of_bombs;
	bool found = false;
        int bomb_location_info[4][4];
        int known_location_info[4][4];

	if (argc == 3)
	{
		sscanf (argv[2],"%d",&n_of_bombs);
		if(n_of_bombs >=2 && n_of_bombs <=4)
		{
			struct locations *bombs = (struct locations *) malloc (sizeof(struct locations)* n_of_bombs);
			

			for(i = 0; i<n_of_bombs; i++)
			{
				bombs[i].x =  rand() % (4-0);
				bombs[i].y =  rand() % (4-0);
				printf("row: %d  -- col: %d \n", bombs[i].y, bombs[i].x);
				bombs[i].found = false;
				if (checkIfBombsWhereRepeated(bombs[i].x, bombs[i].y, bombs, i) == true)
				{
					i--;
				}

			}
			
			
			generateGrid(bombs, bomb_location_info, n_of_bombs);

			for( i= 0; i< (sizeof(known_location_info))/(sizeof(known_location_info[0])); i++)
			{
				for(j= 0; j< sizeof(known_location_info[0])/sizeof(int); j++)
				{
					known_location_info[i][j] = UNKNOWN;
				}

			}


			if( strcmp("clues", argv[1]) == 0)
			{
				int cluesReveled = 0;
				while( cluesReveled < 2)
				{
					int tempx = 0 + rand() % (4-0);
					int tempy = 0 + rand() % (4-0);
					if(!checkNotBoom(tempx, tempy ,  bombs, n_of_bombs))
					{
						update_known_info(tempy, tempx, bomb_location_info, known_location_info);
						cluesReveled++;
					}


				}
				start_game(bombs, bomb_location_info,4 , known_location_info ,n_of_bombs, 3);

			}
			else if(strcmp("noclues", argv[1]) == 0)
			{
				start_game(bombs, bomb_location_info,4 , known_location_info ,n_of_bombs, 5);

			}
			free(bombs);
		}
		else
		{
			printf("You have written an incorrect number of bombs, should be from 2 to 4 bombs.\n");
		}
	}
	else
	{
		printf("You need to write exactly 3 arguments: the executable, the clues/noclues and the number of bombs(2-4).\n");
	}
}



