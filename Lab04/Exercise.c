

#include <stdio.h>
#include <stdlib.h>



void print_nums( int * pNum){
	int i;
	for(i=4; i>=0; i--){
                printf( "%d - ", *(pNum+i));
        }
        printf("\n");

}


void main(){
	int list1[] = {1,2,3,4,5};
	printf("Location of the array: %p \n", &list1[0]);

	int list2[] = {6,7,8,9,10};
	int i;

	for(i= 0; i< sizeof(list1)/sizeof(int); i++){
		list2[i] = list1[i];
	}

	for(i = 0; i< sizeof(list1)/sizeof(int); i++){
		printf("Number %d: %d \n", i, list2[i]);
	}

	int * pNum = malloc(5);

	printf("Location of pNum: %p \n", pNum);

	for(i= 1; i<= 5; i++){
		*(pNum+(i-1))= i*2;
	}

	print_nums(pNum);
}




