
#include <stdio.h>
#include <limits.h>
int main()
{
	/*

	void *stack_ptr;
	asm volatile ("mov %%ebp,%0;" : "=g" (stack_ptr));
	printf("Value of EBP register is %p\n", stack_ptr);
*/
	int num1 = 0xFFFF;
	int num2  = 3;
	char letter1 = 'A';
	char letter2 = 'B';

	printf("Start location of num is 0x%p \n", &num1);
	printf("Start location of num is 0x%p \n", &num2);
	printf("Start location of letter1 is 0x%p \n", &letter1);
	printf("Start location of letter2 os 0x%p \n", &letter2);
	
	printf("Num 2 : %d :%x ", num2, num2);

	printf("Letter 1 : %d: %x: %c \n", letter1, letter1, letter1 );
	printf("Letter 2 : %d: %x: %c  \n", letter2, letter2, letter2 );
	printf("Number of byter: %d , and bits %d \n", sizeof(letter1), sizeof(letter1)*8);
	printf("Max possible value %d \n", CHAR_MAX);
	printf("Min possible value %d \n", CHAR_MIN);

	printf("Num1  : %d : %x \n", num1 ,num1 );
	printf("Num1 bytes: %d, bits: %d \n", sizeof(num1), sizeof(num1)*8);
	printf("Max/Min values: %d / %d \n", INT_MAX, INT_MIN);
    

	printf("Press Return to Continue");
	getchar();
	return 0;
}

