


#include <stdio.h>
#include <ctype.h>


void showLetterLocation( char * pLetter )
{

	printf("The address of the parameter 0x%p, and the value is %d \n", pLetter, *pLetter);
}	

void dobleIt (int* theNumber){
	*theNumber = *theNumber * 2;
}

char toUpperCase(char x ){

	return toupper(x);

}

void main() {

	char theTestChar =  'x';
	showLetterLocation(&theTestChar);
	int testNumber = 5;
	printf("The original number is %d \n", testNumber);
	dobleIt(&testNumber);
	printf("The double of the number is %d \n" , testNumber);
	printf("The upper case of c is %c  \n", toUpperCase('c'));


	int bytesNeededForAddress =   sizeof(void *);
	printf("\n\n The number of bytes needed to store a pointer is %d byte \ns", bytesNeededForAddress);
}
