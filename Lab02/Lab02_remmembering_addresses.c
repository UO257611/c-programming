

#include <limits.h>

void main(){
	int * address1;
	int * address2;
	int * address3;
	
	int num1 = 99;
	int num2 = 22;	
	int* pAddressOfNum1 =-1;
	int* pAddressOfNum2 = -1;
	pAddressOfNum1 = &num1;
	pAddressOfNum2 = &num2;
	
	printf("\n pAddressOfNum1 stores the address %x\n", pAddressOfNum1);
	printf("\n pAddressOfNum2 stores the address %x\n", pAddressOfNum2);

	printf("\n pAddressOfNum1 points to the value %d\n", *pAddressOfNum1);

	printf("\n pAddressOfNum2 points to the value %d\n", *pAddressOfNum2);

}
