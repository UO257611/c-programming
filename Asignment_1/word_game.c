
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "word_game.h"

const char DASH = '-';
const char SPACE = ' ' ;


/*
 *This functions is used to display into the screen the current game state
 * */
void display_game_state(char* pCurrent_letters,const int NUMBER_OF_LETTERS,const int cursor_position, const int number_of_letter_changes, char * pTargetLettters, int* number_of_matches)
{
     printf(" ---Game State --- \n");
     int i;
    //The Target word
     printf("Target: ");
     for(i = 0; i< NUMBER_OF_LETTERS; i++)
     {
	printf("%c", *(pTargetLettters + i)) ;
     }
     printf("\n");

    //The current word that we are modifying 
     for (i = 0 ; i< NUMBER_OF_LETTERS; i++){
	printf("%c" , *(pCurrent_letters+i));
     }
     printf("\n");
     //The cursor
     for( i = 0; i< NUMBER_OF_LETTERS; i++){
     	if(i == cursor_position){
		printf("%c", DASH);
	}
	else{
		printf("%c", SPACE);
	}
     }	
     printf("\nNumber of matched letters %d", *number_of_matches);
     printf("\nChanges: %d \n", number_of_letter_changes);
     
}

/**
 *This function move the cursor to the right, if is not in the last character position
 * */
void move_cursor_right(int* pPosition_of_cursor, const int max)
{
	if( *pPosition_of_cursor < max -1 )
		(*pPosition_of_cursor)++ ;

}

/**
 * This method move the cursor to the left, if it is possible
 * */
void move_cursor_left(int* pPosition_of_cursor, const int max)
{
        if( *pPosition_of_cursor >0)
                (*pPosition_of_cursor)--;

}

/**
 *  This changes the later if it is in the correct boundaries 
 * */
void change_letter(char* pLetters, int position, char new_letter, int max)
{
	if(position < max && position >=0)
		*(pLetters+position) = new_letter;
}

/**
 * This compares the letters of the two words and checks if they are the same
 * and also counts the number of letter that are matching
 * */
bool compare_letters(char* pStart_letters, char* pTarget_letters, int size_of_array, int *  number_of_matches)
{
	int counter = 0;
	bool areSame = true;
	while(counter < size_of_array)
	{
		//If they are not the same change the 'areSame' flag
		if(  *pStart_letters != *pTarget_letters)
			areSame = false;
		//Else increase the number of matchers through the pointer
		else
			*number_of_matches +=1;

		pStart_letters++;
		pTarget_letters++;
		counter++;
	}
	printf("\n");


	return areSame;


}

/**
 * This function saves the words that the user tries in the heap
 **/
void save_word(char** pointerList, char* current_word, const int size_of_word)
{
	//Use malloc in order to allocate space dinamically
	char*  copy = (char*) malloc(size_of_word);
	int i;
	for(i= 0; i< size_of_word; i++){
		//Save the state of the word
		*(copy+i) = *(current_word+i);

	}

	*pointerList = copy;


}

/**
 * Function called in order to start the game
 **/
void start_game(char * pCurrent,  char* pTarget, const int size_of_word)
{
    int position_of_cursor = 0;
    bool game_finished = false;
    int number_of_letter_changes = 0;
    int number_of_matches=0 ;
    //Maximum amount of possible of words to try. 
    //This will store an array of pointers, that points to the stored words
    char* tries[(size_of_word)*2]; 
    int wordCopyCounter = 0;

    while(compare_letters(pCurrent, pTarget, size_of_word, &number_of_matches) == false && number_of_letter_changes < (2*size_of_word))
    {
	    //First, display the game state
	    display_game_state (pCurrent, size_of_word, position_of_cursor, number_of_letter_changes, pTarget, &number_of_matches);
	    number_of_matches = 0;
	    //Second, get the user input and analyze it
	    char x = get_user_char();
	    if( x == '>')
	    {
		    move_cursor_right(&position_of_cursor, size_of_word);
		    number_of_letter_changes++;

	    }
	    else if(x == '<')
	    {
		    move_cursor_left(&position_of_cursor, size_of_word);
		    number_of_letter_changes++;

	    }
	    else if( x >= 97 &&  x <= 122)
	    {

		    change_letter(pCurrent, position_of_cursor, x, size_of_word);
		    save_word(&tries[wordCopyCounter], pCurrent, size_of_word);
		    wordCopyCounter++;
		    number_of_letter_changes++;
	    }
    }
      display_game_state (pCurrent, size_of_word, position_of_cursor, number_of_letter_changes, pTarget, &number_of_matches);


      //Check if the user has won
      if(compare_letters(pCurrent, pTarget, size_of_word, &number_of_matches) == true)
      {
	      printf("\n\n------------------- \n\n");
	      printf("CONGRATULATIONS: YOU WON\n\n");
              printf("------------------- \n\n");

	      //Check if all entrys in the tries array are used
	      //if they are not used we add a termination simbol
	      if(wordCopyCounter != ((size_of_word)*2)-1)
		{
			char last = '!';
			tries[wordCopyCounter] = &last;
		}
	      //Print all the words that have been stored in the heap
	      printWordList(&tries[0],size_of_word*2, size_of_word);

      }
      else
      {
	      //The user is not very smart,
	      //so he has lost the game
	      printf("\n\n------------------- \n\n");
              printf("GAME OVER: YOU LOST\n\n");
              printf("------------------- \n\n");

      }
}

/**
 * This function takes a pointer that points to an array full of the
 * stored words pointers and two constants (the size of each array,
 *  the word stored and the word itself) and it shows them in the screen
 **/
void printWordList(char** pointersToWords,const int size_of_copy, const int size_of_word){
	int counter = 0;
	int i;
	printf("All the words tried:\n");
	while(counter < size_of_copy)
	{
		//Get the word pointer
		char* word = *(pointersToWords+ counter);
		//We arrive to an early end
		if(*word=='!')
			break;
		//Print the word
		for(i= 0; i< size_of_word;i++){
		
			printf("%c", *(word+i));
		}
		//Free the space used to store the word in the heap
		free(word);
		printf("\n");
		counter++;
	}
}



char get_user_char() {
    printf("Enter a command: ");
    char res = getchar();
    bool finish = false;
    char dummy_char = ' ';

    while (finish == false) {
        dummy_char = getchar();
        if (dummy_char == '\n')
            finish = true;
    }
    printf("\n");
    return res;
}

