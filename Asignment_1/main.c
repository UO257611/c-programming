#include <stdio.h>
#include <stdbool.h>
#include "word_game.h"


void main(){


	//Start word and target word
	char current_letters[] = {'b','e'};
	char target_letters[] = {'c','o'};
	int position_of_cursor = 0;
	//Size of the word measured dinamically
	const int NUMBER_OF_LETTERS = sizeof(current_letters)/sizeof(char);
	bool game_finished = false;
	int number_of_letter_changes = 0;
	
	/* ---- TEST CODE ----	
	display_game_state (current_letters, NUMBER_OF_LETTERS, position_of_cursor ,number_of_letter_changes, &target_letters[0]);
	
	move_cursor_right(&position_of_cursor, NUMBER_OF_LETTERS);
	display_game_state (current_letters, NUMBER_OF_LETTERS, position_of_cursor ,number_of_letter_changes, &target_letters[0]);
	
	move_cursor_right(&position_of_cursor, NUMBER_OF_LETTERS);
	display_game_state (current_letters, NUMBER_OF_LETTERS, position_of_cursor ,number_of_letter_changes, &target_letters[0]);
	
	move_cursor_left(&position_of_cursor, NUMBER_OF_LETTERS);
	display_game_state (current_letters, NUMBER_OF_LETTERS, position_of_cursor ,number_of_letter_changes, &target_letters[0]);


	bool x =  compare_letters(&current_letters[0], &target_letters[0], sizeof(current_letters)/sizeof(char));

        printf("%s", x ? "true": "false");


        printf("\n");


	int i ;

	for( i = 0; i < sizeof(target_letters)/ sizeof(char); i+=1){
		change_letter( &current_letters[0], i, target_letters[i],NUMBER_OF_LETTERS );
	}
	 
	display_game_state (current_letters, NUMBER_OF_LETTERS, position_of_cursor ,number_of_letter_changes, &target_letters[0]);


	x =  compare_letters(&current_letters[0], &target_letters[0], sizeof(current_letters)/sizeof(char));

	printf("%s", x ? "true": "false");
*/
	start_game( &current_letters[0], &target_letters[0], NUMBER_OF_LETTERS);

	printf("\n");
	}



