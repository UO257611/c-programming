


#include <stdio.h>
#include <ctype.h>

char* pLetter = NULL;
char letters[] = {'f','r','d','d'};
int numbers[] = {2,4,6,8};

void change_letter(char* myLetters , int pos, char new_letter, int size){

	if(pos < size){

		*(myLetters +pos) = new_letter;

	
	}
}


void to_uppercase(char* myLetters, int max)
{

	int i;
	for(i = 0; i< max; i++){
	
		*(myLetters+i) = toupper(*(myLetters+i));
	}
}


void displayNumbers(int* numbers, int max){
	int i;
	for(i = 0; i<max; i++){
		printf("The %d number is: %d \n", i, *(numbers+i));
	}

}


void display_letters(char* myLetters, int MAX_CHAR_ARRAY_SIZE){
	int i;
	for(i=0; i< MAX_CHAR_ARRAY_SIZE; i++){
		printf("The %d letter is: %c \n ",i, *(myLetters + i));

	}


}


void main(){

	pLetter = &letters[0];

	printf("It array starts at: %p \n", pLetter); 
	
	pLetter = &letters[1];
	printf("The second element starts at: %p \n ", pLetter);

	pLetter =&letters[0];

	int i;
	for(i=0; i<=3;i++){
		printf("The element in position %d that is in the address %p is: %c \n",i,pLetter,*pLetter);
		pLetter +=1;

	}
	printf("------------------------------------------------\n");	
	for(i=0; i<sizeof(numbers)/sizeof(int);i++){
		printf("Address of item %d in the array is %p and contains %d \n", i, &numbers[i], numbers[i]);
	}

	printf("-----------Now using pointers -------------- \n");

	int* pNum = NULL;
	pNum = numbers;
	for(i=0; i<sizeof(numbers)/sizeof(int);i++){
                printf("Address of item %d in the array is %p and contains %d \n", i, pNum+i, *(pNum+i));
        
	int nums[] = { 5,3,2,1};
	const int MAX = 4;
	displayNumbers(nums, MAX);

	char myLetters[] = {'d','i','r','t'};
	const int MAX_CHAR_ARRAY_SIZE = 4;
	display_letters(myLetters, MAX_CHAR_ARRAY_SIZE);


	change_letter(myLetters, 1, 'e', MAX_CHAR_ARRAY_SIZE);
	display_letters(myLetters, MAX_CHAR_ARRAY_SIZE);
		
	  printf("-----------Now to to_uppercase -------------- \n");
	
	to_uppercase(myLetters,MAX_CHAR_ARRAY_SIZE);






}
